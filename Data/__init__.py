__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython', 'C']
__version__ = 1.0
__distributor__ = ['CoreSEC Softwares, CA']
__license__ = ['MIT']
__website__ = 'http://www.coresecsoftware.com/markie'


# { Milestones
# } Milestones


# { Imports
# } Imports


# { Souce
# } Source  