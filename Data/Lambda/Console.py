import platform, subprocess

class Console(object):
    def __init__(self,source=False):
        self.source = source
        self.cache_folder = "Data/Lambda/Cache/"
        self.cache_console = []

    def info(self):
        _i = (
            platform.python_version(),
            platform.python_compiler(),
            platform.python_build()
                )
        _string = """
Version: Python {v}
Compiler: {c}
Build: {b}
""".format(
            v = _i[0],
            c = _i[1],
            b = _i[2]
                        )
        return _string

    def clean_source(self):
        self.cache_console.append(str(self.source).replace("Console: ",""))


    def run(self,python=False):
        try:
            self.clean_source()
        finally:
            #return self.ca[-1]
            pass
