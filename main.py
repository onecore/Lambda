__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__version__ = '0.1'
__distributor__ = ['CoreSEC Softwares, CA']
__license__ = ['MIT']
__info__ = "Python Integrated Development Environment On the fly"
__title__ = "Lambda Edit"

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.codeinput import CodeInput
from kivy.uix.listview import ListView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.parser import parse_color as setcolor
from kivy.uix.scrollview import ScrollView
from kivy.graphics import *
from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.widget import Widget
from kivy.app import App
from Data.Lambda import Console
import subprocess,sys,os

sys.dont_write_bytecode = True

class LambdaMain(App):
    "Main Lambda IDE App Class"

    def __init__(self):
        #// Misc
        self.built = None
        self.cons_cleared = 0

        #// Widgets
        self.Source = CodeInput()
        self.MainBG = Button()
        self.Play = Button()
        self.Offsetter = Label()
        self.OptionObj = Button()
        self.TermObj = Button()
        self.ConsObj = TextInput()
        self.Term_FLObj = FloatLayout()
        self.Term_FLBG = Button()
        self.FileOptionObj = Button()

        #// Options
        self.opt_osys = (sys.platform,"LOCAL")
        self.opt_compile = 0
        self.opt_interpreter = "python"
        self.conf_current = None # Integers / Current Page Index
        self.conf_bgcolor = "#000000"
        self.conf_seperator_color = "#F0F0F0"
        self.offset_show = False
        self.console_showed = False
        self.lang_set = "EN"
        self.lang = None


    #// Functions / Callbacks
    def LoadLangs(self):
        pass

    def LoadOptions(self,lang_get=None):
        "Options Loader (Initialize All)"
        pass

    def AutoCompile(function):
        "Build the Source Automatically"
        def Wrap(*args):
            _f = function
        return Wrap

    def AutoShowPanel(self,cont):
        global lnum, offset

    def OffSetter(self,cont,cont2):
        Offset_x = self.Source.cursor_row + 1
        Offset_y = self.Source.cursor_col + 1
        self.OtherWidgets(show=False,direct=True,x=Offset_x,y=Offset_y)


    def PanelShow(self):
        '''Simple Panel view with some useful infos
        related with the Source
        '''


        self.ConsObj.disabled = True
        self.ConsObj.size_hint = 1,.28
        self.ConsObj.background_color = setcolor("#292929")
        self.ConsObj.multiline = False
        self.ConsObj.foreground_color = setcolor("#33CC00")
        self.Term_FLBG.disabled = True
        self.Term_FLBG.size_hint = 1,.4

        if self.console_showed: #// Closed
            self.TermObj.text = u"\uf102"
            self.TermObj.color = setcolor("#989898")

            self.FileOptionObj.color = setcolor("#989898")

            self.ConsObj.pos_hint = {"center_x":1,"center_y":0013} #// way down..
            self.Term_FLBG.pos_hint = {"center_x":1,"center_y":00013} #// way down..
            self.console_showed = False
            self.Term_FLObj.remove_widget(self.Term_FLBG)
            self.Term_FLObj.remove_widget(self.ConsObj)

        else: #// Opened
            self.TermObj.text = u"\uf103"
            self.TermObj.color = setcolor("#292929")

            self.FileOptionObj.color = setcolor("#292929")

            self.ConsObj.pos_hint = {"center_x":.5,"center_y":.19}
            self.Term_FLBG.pos_hint = {"center_x":.5,"center_y":.14}
            self.console_showed = True

            self.Term_FLObj.add_widget(self.Term_FLBG)
            self.Term_FLObj.add_widget(self.ConsObj)


    def OptionShow(self):
        'Shows a modal with Options'
        pass

    #// Callback Init
    def LoadCallbacks(self):
        self.TermObj.on_press = self.PanelShow #// Terminal
        self.OptionObj.on_press = self.OptionShow #// Options

    #// Contents (Frames / Widget containers)
    def OptionsFrame(self):
        "Options contents"
        pass

    def OtherWidgets(self,show=True,**kwargs):
        "Right bar contents"
        font_icon = "Data/Lambda/Res/fontawesome.ttf" # Font-Awesome Web-Font
        font_color = setcolor("#989898")

        _transparent = (0,0,0,0)

        self.Source.size_hint_y = .95
        self.Source.pos_hint = {"center_x":.5,"center_y":.525}

        if show: #// On Init

            self.FileOptionObj.font_name = font_icon
            self.FileOptionObj.text = u'\uf039'
            self.FileOptionObj.pos_hint = {"center_x":0.026,"center_y":0.025}
            self.FileOptionObj.background_color = _transparent
            self.FileOptionObj.font_size = 20
            self.FileOptionObj.color = font_color

            self.OptionObj.font_size = 38
            self.OptionObj.pos_hint = {"center_x":0.97,"center_y":0.96}
            self.OptionObj.size_hint = .03,.03
            self.OptionObj.font_name = font_icon
            self.OptionObj.text = u'\uf013' # Gear Icon
            self.OptionObj.color = font_color
            self.OptionObj.background_color = _transparent

            self.Offsetter.color = font_color
            self.Offsetter.font_size = 12
            self.Offsetter.pos_hint = {"center_x":.92,"center_y":.03}

            self.TermObj.font_name = font_icon
            self.TermObj.color = font_color
            self.TermObj.background_color = _transparent
            self.TermObj.text = u'\uf102'
            self.TermObj.size_hint = .03,.03
            self.TermObj.pos_hint = {"center_x":.97,"center_y":.028}

        if not show and kwargs:
            self.Offsetter.text = "{ln}/{xy}".format(ln=kwargs["x"],xy=kwargs["y"])

    def Message(self):
        '''
        Message Pop-up with contents
        '''
        pass

    def PopFrame(self):
        "Pop-up bar contents"
        pass

    def MagnifyFrame(self):
        "Magnifier (Right Panel)"
        main = FloatLayout()        #// Main Frame
        main.size_hint_x = .2

        numbConts = ListView()
        for i in range(1,200):
            numbConts.add_widget(Button(text=str(i)))
        bg = Button(size_hint_x=.38) #// Background
        bg.pos_hint = {"center_x":0.12}
        bgdisabled = bg.background_disabled_down
        bg.background_normal,bg.background_down = bgdisabled,bgdisabled
        bg.on_touch_move= self.showMagnifier

        main.add_widget(bg)
        main.add_widget(numbConts)
        return main

    def build(self):
        "Build function for initializing Widgets (Frame)"
        self.LoadOptions()
        self.title = __title__

        self.MainBG.disabled = True
        self.MainBG.disabled_color = setcolor("#")

        self.MainFrame = FloatLayout()
        self.Source.bind(text= self.OffSetter)
        self.MainFrame.add_widget(self.MainBG)  #// BG
        self.MainFrame.add_widget(self.Source)     #// Source View
        self.MainFrame.add_widget(self.OptionObj)  #// Options View
        self.MainFrame.add_widget(self.FileOptionObj) #// File Options View

        if not self.offset_show:
            self.MainFrame.add_widget(self.Offsetter)  #// Offsets View

        self.MainFrame.add_widget(self.Term_FLObj)    #// Console View
        self.MainFrame.add_widget(self.TermObj)    #// Console Button
        self.LoadCallbacks()
        self.OtherWidgets(True)

        return self.MainFrame


if __name__ == '__main__':
    LambdaMain().run()  # Start the Lambda App



