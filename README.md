<p align="center">  <img src="https://gitlab.com/uploads/project/avatar/121889/logo.png"  </p>
--------
**Lambda IDE** a C/C++ and Python Integrated Development Environment<br>
Lambda IDE Supports and Integrates with frameworks for Python, such as Flask for Web Development, PyQt4 for Graphical User Interface Development and more.

* Cross-Platform, Supports (Mac, Linux, Windows, Androids and iOS)
* Implemented using Kivy framework (OpenGL ES) <br>
* Uses Pygment for Syntax Highlighting.
* Written in Python and (Cython/C/C++) Programming Languages

Special thanks to JetBrain's PyCharm for licensing this Open-Source Project (Ironic, IDE for IDE)


|   Version      |   Release Date   |
| -------------  |   -------------  |
|   0.1x         |    Oct 18, 2014  |
|   0.1          |      Under Development            |
----------